#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query_size, struct block_header* block) {
    return block->capacity.bytes >= query_size;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

static void block_init(void* restrict addr, block_size block_sz, void* restrict next) {
    *((struct block_header*)addr) = (struct block_header){
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query_capacity) { return size_max(round_pages(query_capacity), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region* r);

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap((void*)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

static struct region alloc_region(void const* addr, size_t query_capacity) {
    block_size region_size = (block_size){.bytes = region_actual_size(query_capacity)};
    void* mapped_addr = map_pages(addr, region_size.bytes, MAP_FIXED_NOREPLACE);

    if (mapped_addr == MAP_FAILED || mapped_addr != addr) {
        mapped_addr = map_pages(addr, region_size.bytes, 0);
        if (mapped_addr == MAP_FAILED) {
            perror("mmap failed");
            return (struct region){MAP_FAILED, 0, false};
        }
    }

    block_init(mapped_addr, region_size, NULL);

    return (struct region){mapped_addr, region_size.bytes, true};
}

static void* block_after(struct block_header const* block) {
    return (void*)(block->contents + block->capacity.bytes);
}

void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region))
        return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

static bool block_splittable(struct block_header* restrict block, size_t query_capacity) {
    return block->is_free && query_capacity + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query_capacity) {
    if (!block || !block_splittable(block, query_capacity))
        return false;

    block_size next_size = {block->capacity.bytes - query_capacity};

    void* next_block = (void*)((uint8_t*)block + offsetof(struct block_header, contents) + query_capacity);
    block_init(next_block, next_size, NULL);
    block->capacity = (block_capacity){.bytes = query_capacity};
    block->next = next_block;

    return true;
}

static bool blocks_continuous(struct block_header const* fst, struct block_header const* snd) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header* block) {
    struct block_header* next_block = block->next;
    if (next_block && mergeable(block, block->next)) {
        block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
        block->next = next_block->next;
        return true;
    }
    return false;
}

struct block_search_result {
    enum
    {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED
    } type;
    struct block_header* block;
};

static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz) {
    if (!block) {
        return (struct block_search_result){BSR_CORRUPTED, NULL};
    }

    struct block_header* current_block = block;
    struct block_header* last_block = block;

    while (current_block) {
        while (try_merge_with_next(current_block));

        if (current_block->is_free && block_is_big_enough(sz, current_block)) {
            return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, current_block};
        }

        last_block = current_block;
        current_block = current_block->next;
    }

    return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, last_block};
}

static struct block_search_result try_memalloc_existing(size_t query_capacity, struct block_header* block) {
    struct block_search_result found_block = find_good_or_last(block, query_capacity);
    if (found_block.type == BSR_FOUND_GOOD_BLOCK) {
        if (split_if_too_big(found_block.block, query_capacity)) {
            found_block.block->is_free = false;
        }
    } else if (found_block.type == BSR_REACHED_END_NOT_FOUND && found_block.block == NULL) {
        // Если last равен NULL, то куча пуста, возвращаем NULL
        return (struct block_search_result){BSR_CORRUPTED, NULL};
    }
    return found_block;
}

static struct block_header* grow_heap(struct block_header* restrict last, size_t query_capacity) {
    block_size region_size = (block_size){.bytes = region_actual_size(query_capacity)};
    void* mapped_addr = map_pages(block_after(last), region_size.bytes, 0);

    struct region new_region = alloc_region(mapped_addr, query_capacity);

    if (region_is_invalid(&new_region))
        return NULL;

    last->next = new_region.addr;
    if (try_merge_with_next(last)) {
        return last;
    } else {
        return new_region.addr;
    }
}

static struct block_header* memalloc(size_t query_capacity, struct block_header* heap_start) {
    struct block_search_result found_block = try_memalloc_existing(query_capacity, heap_start);

    if (found_block.type == BSR_CORRUPTED || found_block.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header* new_block = NULL;

        if (found_block.type == BSR_CORRUPTED) {
            // Сначала сохраняем указатель на старую кучу
            struct block_header* old_heap = heap_start;

            new_block = heap_init(query_capacity);

            if (new_block != NULL) {
                // Если успешно создана новая куча, возвращаем указатель на старую
                return old_heap;
            }
        } else if (found_block.type == BSR_REACHED_END_NOT_FOUND) {
            new_block = grow_heap(found_block.block, query_capacity);
        }

        if (new_block != NULL) {
            split_if_too_big(new_block, query_capacity);
            new_block->is_free = false;
            return new_block;
        }

    } else if (found_block.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(found_block.block, query_capacity);
        found_block.block->is_free = false;
        return found_block.block;
    }
    return NULL;
}

void* _malloc(size_t query_size) {
    // Проверка на нулевой размер запроса
    if (query_size == 0) {
        return NULL;
    }

    struct block_header* const addr = memalloc(query_size, (struct block_header*)HEAP_START);
    return addr ? addr->contents : NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*)(((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free(void* mem) {
    if (!mem)
        return;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
